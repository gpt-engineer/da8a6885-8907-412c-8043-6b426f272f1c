document.addEventListener("DOMContentLoaded", function () {
  loadProducts();
  // Retrieve mode from local storage
  var wishlistMode = localStorage.getItem("wishlistMode") === "true";
  document.getElementById("wishlist-switch").checked = wishlistMode;
  toggleWishlistMode(wishlistMode);
});

document.getElementById("theme-switch").addEventListener("change", function () {
  document.body.classList.toggle("bg-gray-800");
  document.body.classList.toggle("text-white");
  document.body.classList.toggle("bg-F5E7DE");
  document.body.classList.toggle("text-black");
  // Save the current theme preference in local storage
  localStorage.setItem(
    "userThemePreference",
    document.body.classList.contains("bg-gray-800") ? "dark" : "light",
  );
});

document
  .getElementById("wishlist-switch")
  .addEventListener("change", function () {
    var wishlistMode = this.checked;
    // Store mode in local storage
    localStorage.setItem("wishlistMode", wishlistMode);
    toggleWishlistMode(wishlistMode);
  });

function toggleWishlistMode(wishlistMode) {
  var priceInputs = document.querySelectorAll(".price-input");
  var priceColumns = document.querySelectorAll(".price-column");
  var submitButton = document.getElementById("submit-button");
  var title = document.querySelector("h1");
  var summaryContainer = document.querySelector("aside");
  priceInputs.forEach(function (input) {
    input.style.display = wishlistMode ? "none" : "block";
  });
  priceColumns.forEach(function (column) {
    column.style.display = wishlistMode ? "none" : "table-cell";
  });
  submitButton.textContent = wishlistMode ? "Add to Wishlist" : "Add to Cart";
  title.textContent = wishlistMode ? "Wishlist" : "Universal Shopping Cart";
  summaryContainer.style.display = wishlistMode ? "none" : "block";
}

document
  .getElementById("product-form")
  .addEventListener("submit", function (event) {
    event.preventDefault();
    // Get the product information from the form
    var url = document.getElementById("product-url").value;
    var name = document.getElementById("product-name").value;
    var price = document.getElementById("product-price").value;
    var quantity = document.getElementById("product-quantity").value;
    var currency = document.getElementById("product-currency").value;
    var image = document.getElementById("product-image").value;
    var discount = document.getElementById("product-discount").value;
    // Calculate the final price
    var finalPrice = price - (price * discount) / 100;
    // Add the product to the table
    var img = document.createElement("img");
    img.src = image;
    img.width = 50; // Set the width of the thumbnail
    img.alt = name;
    var link = document.createElement("a");
    link.href = image;
    link.target = "_blank"; // Open the link in a new tab
    link.appendChild(img);
    var imgCell = document.createElement("td");
    imgCell.appendChild(link);
    var row = document.createElement("tr");
    row.appendChild(imgCell);
    row.innerHTML += `
    <td><a href="${url}" target="_blank">${name}</a></td>
    <td>${price}</td>
    <td>${quantity}</td>
    <td>${currency}</td>
    <td>${discount}%</td>
    <td>${finalPrice}</td>
    <td><i class="far fa-star text-yellow-500 cursor-pointer favorite-icon"></i></td>
    <td><i class="fas fa-times text-red-500 cursor-pointer remove-icon"></i></td>
  `;
    document.getElementById("products-body").appendChild(row);
    // Add event listener to favorite and remove icons
    row.querySelector(".favorite-icon").addEventListener("click", function () {
      this.classList.toggle("far");
      this.classList.toggle("fas");
    });
    row.querySelector(".remove-icon").addEventListener("click", function () {
      var removedRow = this.parentElement.parentElement;
      var finalPrice = parseFloat(removedRow.children[5].textContent);
      var price = parseFloat(removedRow.children[2].textContent);
      var discount = parseFloat(removedRow.children[4].textContent);
      var savings = (price * discount) / 100;
      var totalSum = document.getElementById("total-sum");
      totalSum.textContent = parseFloat(totalSum.textContent) - finalPrice;
      var totalSavings = document.getElementById("total-savings");
      totalSavings.textContent = parseFloat(totalSavings.textContent) - savings;
      removedRow.remove();
    });
    // Update the total sum and savings
    var totalSum = document.getElementById("total-sum");
    totalSum.textContent = parseFloat(totalSum.textContent) + finalPrice;
    var totalSavings = document.getElementById("total-savings");
    totalSavings.textContent =
      parseFloat(totalSavings.textContent) + (price - finalPrice);
    // Clear the input fields
    document.getElementById("product-url").value = "";
    document.getElementById("product-name").value = "";
    document.getElementById("product-price").value = "";
    document.getElementById("product-quantity").value = "";
    document.getElementById("product-currency").value = "";
    document.getElementById("product-image").value = "";
    document.getElementById("product-discount").value = "";
  });
